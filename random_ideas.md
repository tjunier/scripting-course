Tools
=====

* write a simple tokenizer to help see how Bash tokenizes


Topics
======

* variables, parameter expansion, and quoting
* I/O: < > >>; read; printf
* positional parameters
* control structures
* optional parameters
* handy shell tools
* limitations (e.g. no floating-point arithmetic - mention Zsh)
* mktmp

Examples/Exercises
==================

* extend the species counter to other taxonomic rank: could illustrate 
  CLI parameters and quoting
* add a warning for redundant IDs to that script: illustrate error handling and
  arrays.
* Bentley's problem, Knuth's solution, and McIlroy's (https://www.johndcook.com/blog/2019/02/18/command-line-wizard/)
* A simple `seqret`-like script that works on FastA but with a more traditional
  UNIX interface

Misc
====

An older list, from before it became clear thet the course is about _scripting_
in particular (rather than the more generic "advanced shell")

* all shell expansions (parameter, filename, history...) 
* aliases 
* discuss sed, awk, join, find, mlr?
* environment variables: how they work and which are important
* redirection beyond > and  >>: <<, &>, etc.
* job control
* subshells
* rc files
