---
title: Preqrequisites for the SIB Shell Scripting Course
subtitle: What you should already be familiar with
---

Background
==========

The advanced shell course, which covers scripting, relies on material taught in
entry-level courses such as [First Steps with
UNIX](https://www.sib.swiss/training/course/2021-01-unix). To help you decide
whether the advanced course is for you, this document presents a list of notions
with which you should be familiar: if any of the following seems mysterious, you
may wish to take more introductory course before this one.

Notions
=======

You should be able to explain the following in one or two sentences:

Shells in General
-----------------

* What a shell is
* How to launch a shell on your machine
* How one works in the shell, especially as compared with working in a graphical
  environment

Commands
--------

* How to execute shell commands
* How shells communicate results (if any)
* What arguments are and how to pass them
* What options are and how to pass them

The Filesystem
--------------

* How files and directories are organized
* What the current directory is and how to determine it
* The difference between an absolute and a relative pathname
* How to list files in the current directory (and, for that matter, in any
  directory)
* How to move around the filesystem
* How to rename and move files

Text Files
----------

* What a text file is, how it differs from, say, a Word document, and how this
  relates to shell commands

Redirection and Pipelines
-------------------------

* How to redirect commands' input and output to/from files
* How to pass the output of one command to another command's input
* What a pipeline is

Familiar Shell Tools
====================

You should know the purpose of the following tools, and be able to understand
what they do (though in-depth knowledge is not required):

* `cut`
* `find`
* `grep`
* `head`
* `man`
* `sort`
* `tail`
* `tr`
* `wc`

Text Editor
===========

We'll be writing code, so you need to be comfortable with a text editor/IDE. 
