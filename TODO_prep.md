1. Rework the order so that tests can be introduced earlier
1. discuss when not to use Bash vs e.g. Python (see src/ntcount*.*) - try
   ```
   $ for count in ntcount*; do
      echo "$count"
      time "./$count" < big_genome
    done
   ```
   This shows that a pure-Bash solution is the slowest, while a solution
   involving Bash calling other programs (which is how Bash is meant to be used)
   is the fastest (by several orders of magnitude).
1. find an image of medieval manuscript without spaces, to illustrate lexical
   analysis (tokenizing)
1. [x] reinstate examples of VCF, etc (needed for "Why?" slide)
1. [x] mention that shells are also a programming language
1. [x] be consistent in '>', vs >, etc.
1. [x] italics are often not very visible: use bold?
1. [x] fasta2csv should end its output with a newline
1. [x] make or find `sample_00.dna`, used in Automation
