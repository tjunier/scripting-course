.PHONY: slides exercises rush

all: slides rush exercises self-test.pdf

slides:
	$(MAKE) -C ./slides/2days
	$(MAKE) -C ./slides/3days

rush:
	$(MAKE) -C ./src
	install -D -m 500 ./src/rush ./bin/rush

exercises:
	$(MAKE) -C ./exercises

exam_questions.asc: exam_questions.md
	gpg -r v10 -a -e $<

exam_questions.pdf: exam_questions.md Makefile
	pandoc --standalone --metadata-file ~/.mdpdf.yaml \
		--to=latex --output $@ $<

self-test.pdf: self-test.md
	pandoc --standalone --metadata-file ~/.mdpdf.yaml --to=latex \
	--filter pandoc-crossref --citeproc --output $@ $<

SIB-bash-scripting-course.tar.gz: slides rush exercises
	tar hczf $@ SIB-bash-scripting-course

mrproper: 
	$(RM) shell-scripting_beamer.pdf exam_questions.pdf *.tar.gz
